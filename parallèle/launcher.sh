#!/bin/bash

nb_ranks=$(lscpu | grep Processeur | cut -d':' -f2)
nb_cores=$(lscpu | grep cœur : | cut -d':' -f2)
echo $nb_ranks
echo $nb_cores
make clean
make

#ecrire sur le fichier config
echo "#" > config
echo "nb_ranks = $nb_ranks;" >> config

echo >> config
echo "#" >> config
echo "nb_cores = $nb_cores;" >> config

echo >> config
echo "#H" >> config
echo "in_dir_H   = { $(ls -R | grep /seq | grep H | cut -d':' -f1); };" >> config
echo "in_files_H = { liste des fichiers; };" >> config

echo >> config
echo "#P" >> config
echo "in_dir_P   = { $(ls -R | grep /seq | grep P | cut -d':' -f1); };" >> config
echo "in_files_P = { liste des fichiers; };" >> config

echo >> config
echo "#brca1" >> config
echo "in_dir_b1   = { $(ls -R | grep /brca1 | cut -d':' -f1) };" >> config
echo "in_files_b1 = {liste des fichiers; };" >> config

echo >> config
echo "#brca2" >> config
echo "in_dir_b2   = { $(ls -R | grep /brca2 | cut -d':' -f1) };" >> config
echo "in_files_b2 = {liste des fichiers; };" >> config

echo >> config
echo "#pcat1" >> config
echo "in_dir_p1   = { $(ls -R | grep /pcat1 | cut -d':' -f1) };" >> config
echo "in_files_p1 = {liste des fichiers; };" >> config

echo >> config
echo "#Rapport" >> config
echo "out_report  = \"analysis_report.txt\";" >> config

#ecrire sur le fichier rapport:

echo "####"> rapport 
echo "		Exemples sont entre parentheses.
		Les mots cles sont en majuscule, ils doivent apparaitre comme tel dans le rapport.
		Les crochets sont un caractere terminal, ils doivent apparaitre comme tel aussi.">> rapport
echo "####">> rapport 
echo "   [META_RUN: [$(date +%d/%m/%y)] [$(date +%H:%M:%S)]]">>rapport
echo "   [RUN]">> rapport


mpirun -np $nb_ranks ./test $nb_cores
